<?php
return [
    'model' => [
        'delete' => 'Can not delete model maybe has relations',
        'not_found' => 'Model not found',
    ],
];
