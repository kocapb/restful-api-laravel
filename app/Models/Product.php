<?php


namespace App\Models;


use Illuminate\Database\Eloquent\{Factories\HasFactory, Model, SoftDeletes};
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Collection;
use Exception;

/**
 * Class Order
 * @property integer $id
 * @property string $name
 * @property string $price
 * @property boolean $published
 * @package App\Models
 */
class Product extends Model
{
    use SoftDeletes, HasFactory;

    /** @var bool */
    const UNPUBLISHED = false;
    /** @var bool  */
    const PUBLISHED = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'published'
    ];

    /**
     * @var false[]
     */
    protected $attributes = [
        'published' => self::UNPUBLISHED,
    ];

    /**
     * @return BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories' , 'product_id', 'category_id');
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories()
    {
        return $this->categories()->get();
    }
}
