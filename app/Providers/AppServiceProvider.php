<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Money
         */
        $this->app['validator']->extend('money', function ($attribute, $value) {
            return preg_match('/^\d*(\.\d{1,2})?$/', $value);
        });
    }
}
