<?php


namespace App\Repositories\Criteria\Product;


use App\Repositories\Contracts\RepositoryInterface as Repository;
use App\Repositories\Criteria\Criteria;
use Illuminate\Support\Facades\DB;

/**
 * Class SearchByCategoryIdCriteria
 * @package App\Repositories\Criteria\Product
 */
class SearchByCategoryIdCriteria extends Criteria
{
    /** @var int */
    protected $categoryId;

    /**
     * SearchByCategoryIdCriteria constructor.
     * @param int $categoryId
     */
    public function __construct(int $categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->select(DB::raw('DISTINCT(products.id)'), 'products.*')
            ->join('product_categories', 'products.id', 'product_categories.product_id')
            ->where('product_categories.category_id', '=', $this->categoryId);
    }
}
