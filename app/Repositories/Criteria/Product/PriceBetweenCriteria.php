<?php


namespace App\Repositories\Criteria\Product;


use App\Repositories\Contracts\RepositoryInterface as Repository;
use App\Repositories\Criteria\Criteria;

/**
 * Class PriceBetweenCriteria
 * @package App\Repositories\Criteria\Product
 */
class PriceBetweenCriteria extends Criteria
{
    /** @var float|null */
    protected $priceFrom;
    /** @var float|null */
    protected $priceTo;

    /**
     * PriceBetweenCriteria constructor.
     * @param float|null $priceFrom
     * @param float|null $priceTo
     */
    public function __construct(float $priceFrom = null, float $priceTo = null)
    {
        $this->priceFrom = $priceFrom;
        $this->priceTo = $priceTo;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        if ($this->priceFrom)
        {
            $model = $model->where('price', '>=', $this->priceFrom);
        }
        if ($this->priceTo)
        {
            $model = $model->where('price', '<=', $this->priceTo);
        }

        return $model;
    }

}
