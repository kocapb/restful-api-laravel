<?php


namespace App\Repositories\Criteria\Product;


use App\Repositories\Contracts\RepositoryInterface as Repository;
use App\Repositories\Criteria\Criteria;
use Illuminate\Support\Facades\DB;

/**
 * Class SearchByNameCriteria
 * @package App\Repositories\Criteria\Product
 */
class SearchByNameCriteria extends Criteria
{
    /** @var string  */
    protected $name;

    /**
     * SearchByNameCriteria constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = strtolower($name);
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->where(DB::raw("lower(name)"), 'like', "%{$this->name}%");
    }
}
