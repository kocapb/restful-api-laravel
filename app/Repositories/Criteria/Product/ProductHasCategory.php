<?php


namespace App\Repositories\Criteria\Product;


use App\Repositories\Contracts\RepositoryInterface as Repository;
use App\Repositories\Criteria\Criteria;

/**
 * Class ProductHasCategory
 * @package App\Repositories\Criteria\Product
 */
class ProductHasCategory extends Criteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->select('products.*')
            ->join('product_categories', 'products.id', 'product_categories.product_id');
    }

}
