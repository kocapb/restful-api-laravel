<?php


namespace App\Repositories\Criteria\Category;


use App\Repositories\Contracts\RepositoryInterface as Repository;
use App\Repositories\Criteria\Criteria;

/**
 * Class CategoryWithProducts
 * @package App\Repositories\Criteria\Category
 */
class CategoryWithProducts extends Criteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        return $model->select('categories.*')
            ->join('product_categories', 'categories.id', 'product_categories.category_id');
    }

}
