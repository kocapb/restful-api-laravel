<?php


namespace App\Repositories\Eloquent;

use App\Models\Category;
use App\Models\Product;

/**
 * Class CategoryRepository
 * @package App\Repositories\Eloquent
 */
class CategoryRepository extends Repository
{
    /**
     * @return mixed|string
     */
    public function model()
    {
        return Category::class;
    }

}
