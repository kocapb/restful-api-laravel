<?php


namespace App\Repositories\Eloquent;

use App\Models\Product;

/**
 * Class ProductRepository
 * @package App\Repositories\Eloquent
 */
class ProductRepository extends Repository
{
    /**
     * @return mixed|string
     */
    public function model()
    {
        return Product::class;
    }

}
