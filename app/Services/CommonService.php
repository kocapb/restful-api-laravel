<?php


namespace App\Services;


use App\Http\Requests\Request;
use App\Repositories\Eloquent\Repository;

/**
 * Class CommonService
 * @package App\Services
 */
abstract class CommonService
{
    /** @var Request  */
    protected $request;
    /** @var Repository  */
    protected $repository;

    /**
     * CommonService constructor.
     * @param Repository $repository
     * @param Request|null $request
     */
    public function __construct(Repository $repository, Request $request = null)
    {
        $this->repository = $repository;
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return mixed
     */
    public function getRepository()
    {
        return $this->repository;
    }


}
