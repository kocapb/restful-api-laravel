<?php


namespace App\Services\Category;

use App\Exceptions\{ModelDeleteException, ModelNotFoundException};
use App\Interfaces\ServiceDeleteInterface;
use App\Models\Category;
use App\Repositories\Eloquent\CategoryRepository;
use App\Services\CommonService;
use Exception;

/**
 * Class CategoryDeleteService
 * @package App\Services\Category
 */
class CategoryDeleteService extends CommonService implements ServiceDeleteInterface
{

    /**
     * CategoryDeleteService constructor.
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @param int $id
     * @return bool|mixed|null
     * @throws ModelDeleteException
     * @throws ModelNotFoundException
     * @throws Exception
     */
    public function delete(int $id)
    {
        $repository = $this->getRepository();
        /** @var Category $category */
        $category = $repository->with(['products'])->find($id);

        if (!$category)
        {
            throw new ModelNotFoundException(trans('exception.model.not_found'));
        }
        if ($category->getProducts()->count())
        {
            throw new ModelDeleteException(trans('exception.model.delete'));
        }
        return $category->delete();
    }
}
