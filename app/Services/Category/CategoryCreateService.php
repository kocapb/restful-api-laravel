<?php


namespace App\Services\Category;


use App\Http\Requests\Category\CategoryCreateRequest;
use App\Http\Requests\Request;
use App\Interfaces\ServiceInterface;
use App\Repositories\Eloquent\CategoryRepository;
use App\Repositories\Eloquent\Repository;
use App\Services\CommonService;

/**
 * Class CategoryCreateService
 * @package App\Services\Category
 */
class CategoryCreateService extends CommonService implements ServiceInterface
{
    /**
     * CategoryCreateService constructor.
     * @param CategoryRepository $repository
     * @param CategoryCreateRequest $request
     */
    public function __construct(CategoryRepository $repository, CategoryCreateRequest $request)
    {
        parent::__construct($repository, $request);
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->getRepository()->create($this->getRequest()->all());
    }
}
