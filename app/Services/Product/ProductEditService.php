<?php


namespace App\Services\Product;


use App\Exceptions\Product\ProductServiceException;
use App\Http\Requests\Product\ProductEditRequest;
use App\Interfaces\ServiceEditInterface;
use App\Models\Product;
use App\Repositories\Eloquent\ProductRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class ProductEditService
 * @package App\Services\Product
 */
class ProductEditService extends ProductCommonService implements ServiceEditInterface
{
    /**
     * ProductEditService constructor.
     * @param ProductRepository $repository
     * @param ProductEditRequest|null $request
     */
    public function __construct(ProductRepository $repository, ProductEditRequest $request = null)
    {
        parent::__construct($repository, $request);
    }

    /**
     * @param $id
     * @return Model
     * @throws ProductServiceException
     */
    public function edit($id): Model
    {
        try {
            DB::beginTransaction();
            $repository = $this->getRepository();
            $repository->update($this->getData(), $id);
            /** @var Product $product */
            $product = $repository->find($id);
            $this->categoryProductSync($product);
            DB::commit();
        }
        catch (Throwable $exception)
        {
            throw new ProductServiceException($exception->getMessage(), $exception->getCode());
        }

        return $product;
    }

}
