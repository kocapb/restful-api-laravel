<?php


namespace App\Services\Product;


use App\Http\Requests\Product\ProductListRequest;
use App\Interfaces\ServiceInterface;
use App\Models\Product;
use App\Repositories\Criteria\Product\OnlyPublishedCriteria;
use App\Repositories\Criteria\Product\PriceBetweenCriteria;
use App\Repositories\Criteria\Product\SearchByCategoryIdCriteria;
use App\Repositories\Criteria\Product\SearchByNameCriteria;
use App\Repositories\Eloquent\ProductRepository;
use App\Services\CommonService;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ProductListService
 * @package App\Services\Product
 */
class ProductListService extends CommonService implements ServiceInterface
{
    /**
     * ProductListService constructor.
     * @param ProductRepository $repository
     * @param ProductListRequest|null $request
     */
    public function __construct(ProductRepository $repository, ProductListRequest $request = null)
    {
        parent::__construct($repository, $request);
    }

    /**
     * @return Collection|Product[]
     */
    public function get()
    {
        /** @var ProductRepository $repository */
        $repository = $this->getRepository()->pushCriteria(new OnlyPublishedCriteria());
        /** @var ProductListRequest $request */
        $request = $this->getRequest();

        if ($request->get('name'))
        {
            $repository = $repository->pushCriteria(new SearchByNameCriteria($request->get('name')));
        }

        if ($request->get('category_id'))
        {
            $repository = $repository->pushCriteria(new SearchByCategoryIdCriteria($request->get('category_id')));
        }

        return $repository->pushCriteria(new PriceBetweenCriteria($request->get('price_from'), $request->get('price_to')))
            ->with(['categories'])
            ->paginate(config('paginate.per_page'));
    }

}
