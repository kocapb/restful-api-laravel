<?php


namespace App\Services\Product;


use App\Models\Product;
use App\Services\CommonService;

/**
 * Class ProductCommonService
 * @package App\Services\Product
 */
abstract class ProductCommonService extends CommonService
{
    /** @var array */
    private $data;

    /**
     * @return array
     */
    public function getData()
    {
        if ($this->data)
        {
            return $this->data;
        }

        $data = $this->getRequest()->all();
        if (isset($data['category_ids']))
        {
            unset($data['category_ids']);
        }

        return $this->data = $data;
    }

    /**
     * @param Product $product
     */
    public function categoryProductSync(Product $product)
    {
        $request = $this->getRequest();
        if ($request->has('category_ids') && !empty($request->get('category_ids')))
        {
            $product->categories()->sync($request->get('category_ids'));
        }
    }
}
