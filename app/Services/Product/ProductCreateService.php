<?php


namespace App\Services\Product;


use App\Exceptions\Product\ProductServiceException;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Interfaces\ServiceInterface;
use App\Models\Product;
use App\Repositories\Eloquent\ProductRepository;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class ProductCreateService
 * @package App\Services\Product
 */
class ProductCreateService extends ProductCommonService implements ServiceInterface
{
    /**
     * ProductCreateService constructor.
     * @param ProductRepository $repository
     * @param ProductCreateRequest|null $request
     */
    public function __construct(ProductRepository $repository, ProductCreateRequest $request = null)
    {
        parent::__construct($repository, $request);
    }

    /**
     * @return mixed
     * @throws ProductServiceException
     */
    public function get()
    {
        try {
            DB::beginTransaction();
            /** @var Product $product */
            $product = $this->getRepository()->create($this->getData());
            $this->categoryProductSync($product);
            DB::commit();
        }
        catch (Throwable $exception)
        {
            DB::rollBack();
            throw new ProductServiceException($exception->getMessage(), $exception->getCode());
        }

        return $product;
    }
}
