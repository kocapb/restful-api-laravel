<?php


namespace App\Services\Product;


use App\Interfaces\ServiceDeleteInterface;
use App\Repositories\Eloquent\ProductRepository;
use App\Services\CommonService;

/**
 * Class ProductDeleteService
 * @package App\Services\Product
 */
class ProductDeleteService extends CommonService implements ServiceDeleteInterface
{
    public function __construct(ProductRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * @param int $id
     * @return mixed|void
     */
    public function delete(int $id)
    {
        return $this->getRepository()->delete($id);
    }
}
