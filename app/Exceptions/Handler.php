<?php

namespace App\Exceptions;

use HttpException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function render($request, Throwable $exception)
    {
        $errors = [];
        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        switch (true) {
            case $exception instanceof HttpException:
                $statusCode = $exception->getCode();
                break;
            case $exception instanceof ValidationException:
                $errors = $exception->errors();
                $statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
                break;
            case $exception instanceof BaseException:
                if ($exception->getCode())
                {
                    $statusCode = $exception->getCode();
                }
                break;

        }

        $responseArray = [
            'success' => false,
            'message' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'errors' => $errors,
        ];

        if (config('app.env') === 'local' && config('app.debug') === true) {
            $responseArray['stacktrace'] = $exception->getTrace();
        }

        if ($request->isJson())
        {
            return response()->json($responseArray, $statusCode);
        }
        return parent::render($request, $exception);
    }
}
