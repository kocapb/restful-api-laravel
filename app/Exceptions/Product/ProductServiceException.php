<?php


namespace App\Exceptions\Product;

use App\Exceptions\BaseException;

/**
 * Class ProductServiceException
 * @package App\Exceptions\Product
 */
class ProductServiceException extends BaseException
{

}
