<?php


namespace App\Exceptions;


use Illuminate\Http\Response;

/**
 * Class ModelNotFoundException
 * @package App\Exceptions
 */
class ModelNotFoundException extends BaseException
{
    protected $code = Response::HTTP_NOT_FOUND;
}
