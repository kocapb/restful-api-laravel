<?php


namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

/**
 * Class BaseException
 * @package App\Exceptions
 */
class BaseException extends Exception
{
    protected $code = Response::HTTP_INTERNAL_SERVER_ERROR;
}
