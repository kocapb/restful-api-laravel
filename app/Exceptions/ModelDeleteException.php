<?php


namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class ModelDeleteException
 * @package App\Exceptions
 */
class ModelDeleteException extends BaseException
{
    protected $code = Response::HTTP_UNPROCESSABLE_ENTITY;
}
