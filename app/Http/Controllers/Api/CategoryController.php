<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\Category\CategoryResource;
use App\Services\Category\{CategoryCreateService, CategoryDeleteService};
use Illuminate\Http\JsonResponse;
use App\Traits\JsonResponseTrait;
use App\Exceptions\{ModelDeleteException, ModelNotFoundException};

/**
 * Class CategoryController
 * @package App\Http\Controllers\Api
 */
class CategoryController extends Controller
{
    use JsonResponseTrait;

    /**
     * @param CategoryCreateService $service
     * @return CategoryResource
     */
    public function create(CategoryCreateService $service)
    {
        return new CategoryResource($service->get());
    }

    /**
     * @param int $id
     * @param CategoryDeleteService $service
     * @return JsonResponse
     */

    /**
     * @param int $id
     * @param CategoryDeleteService $service
     * @return JsonResponse
     * @throws ModelDeleteException
     * @throws ModelNotFoundException
     */
    public function delete(int $id, CategoryDeleteService $service)
    {
        $service->delete($id);
        return $this->successDeleted();
    }
}
