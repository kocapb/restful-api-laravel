<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Product\ProductResource;
use App\Exceptions\Product\ProductServiceException;
use App\Services\Product\{ProductCreateService, ProductDeleteService, ProductEditService, ProductListService};
use App\Traits\JsonResponseTrait;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\JsonResponse;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    use JsonResponseTrait;

    /**
     * @param ProductListService $service
     * @return AnonymousResourceCollection
     */
    public function getList(ProductListService $service)
    {
        return ProductResource::collection($service->get());
    }

    /**
     * @param ProductCreateService $service
     * @return ProductResource
     * @throws ProductServiceException
     */
    public function create(ProductCreateService $service)
    {
        return new ProductResource($service->get());
    }

    /**
     * @param int $id
     * @param ProductEditService $service
     * @return ProductResource
     * @throws ProductServiceException
     */
    public function edit(int $id, ProductEditService $service)
    {
        return new ProductResource($service->edit($id));
    }

    /**
     * @param $id
     * @param ProductDeleteService $service
     * @return JsonResponse
     */
    public function delete($id, ProductDeleteService $service)
    {
        $service->delete($id);
        return $this->successDeleted();
    }
}
