<?php


namespace App\Http\Resources\Category;


use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CategoryResource
 * @mixin  Category
 * @package App\Http\Resources\Category
 */
class CategoryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->name
        ];
    }
}
