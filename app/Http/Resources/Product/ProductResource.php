<?php


namespace App\Http\Resources\Product;


use App\Http\Resources\Category\CategoryResource;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * Class ProductResource
 * @mixin Product
 * @package App\Http\Resources\Product
 */
class ProductResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'price'      => $this->price,
            'published'  => $this->published,
            'categories' => CategoryResource::collection($this->getCategories()),
        ];
    }
}
