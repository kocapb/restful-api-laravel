<?php


namespace App\Http\Requests\Product;


use App\Http\Requests\Request;

/**
 * Class ProductListRequest
 * @package App\Http\Requests\Product
 */
class ProductListRequest extends Request
{
    public function rules(): array
    {
        return [
            'name' => 'string',
            'category_id' => 'integer',
            'price_from' => 'money',
            'price_to'   => 'money',
        ];
    }
}
