<?php


namespace App\Http\Requests\Product;


use App\Http\Requests\Request;

/**
 * Class ProductCreateRequest
 * @package App\Http\Requests\Product
 */
class ProductCreateRequest extends Request
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:3|max:60',
            'price' => 'required|money',
            'category_ids' => 'required|array',
            'published' => 'boolean'
        ];
    }

}
