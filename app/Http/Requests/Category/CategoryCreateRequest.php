<?php


namespace App\Http\Requests\Category;


use App\Http\Requests\Request;

class CategoryCreateRequest extends Request
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:3|max:60|unique:categories,name',
        ];
    }

}
