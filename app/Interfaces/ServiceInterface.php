<?php


namespace App\Interfaces;

/**
 * Interface ServiceInterface
 * @package App\Interfaces
 */
interface ServiceInterface
{
    /**
     * @return mixed
     */
    public function get();
}
