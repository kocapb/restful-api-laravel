<?php


namespace App\Interfaces;

/**
 * Interface ServiceDeleteInterface
 * @package App\Interfaces
 */
interface ServiceDeleteInterface
{
    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id);
}
