<?php


namespace App\Interfaces;


use Illuminate\Database\Eloquent\Model;

/**
 * Interface ServiceEditInterface
 * @package App\Interfaces
 */
interface ServiceEditInterface
{
    /**
     * @param $id
     * @return Model
     */
    public function edit($id) : Model;
}
