<?php


namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Trait JsonResponseTrait
 * @package App\Traits
 */
trait JsonResponseTrait
{
    /**
     * @param array $data
     * @return JsonResponse
     */
    public function success(array $data = [])
    {
        return $this->jsonResponse($data);
    }

    /**
     * @return JsonResponse
     */
    public function successDeleted()
    {
        return $this->jsonResponse([], Response::HTTP_NO_CONTENT);
    }

    protected function jsonResponse(array $data, int $code = Response::HTTP_OK)
    {
        $result = [
            'success' => true,
        ];
        if ($data) {
            $result['data'] = $data;
        }

        return response()->json($result, $code);
    }
}
