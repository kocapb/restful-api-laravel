<?php


namespace Tests\Feature\Api\Category;


use App\Models\Category;
use App\Repositories\Criteria\Category\{CategoryWithoutProducts, CategoryWithProducts};
use App\Repositories\Eloquent\CategoryRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

/**
 * Class DeleteCategoryTest
 * @package Tests\Feature
 */
class DeleteTest extends TestCase
{
    /**
     * @return CategoryRepository
     */
    public function getRepository() : CategoryRepository
    {
        return App::make(CategoryRepository::class);
    }

    /**
     * @return void
     */
    public function testNotFound()
    {
        $id = $this->getRepository()->all()->max('id');
        $this
            ->withHeader('Content-Type', 'application/json')
            ->delete($this->getRouteByName('category.delete', ['id' => $id + 1]))
            ->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJson([
                'message' => trans('exception.model.not_found'),
            ]);

    }

    /**
     * @return void
     */
    public function canNotDeleteBecauseHasRelation() : void
    {
        $category = $this->getRepository()->pushCriteria(new CategoryWithProducts())->all()->first();
        $this->delete($this->getRouteByName('category.delete', ['id' => $category->id]))
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => trans('exception.model.delete'),
            ]);
    }

    /**
     * @return void
     */
    public function testSuccess()
    {
        /** @var Category $category */
        $category = $this->getRepository()->pushCriteria(new CategoryWithoutProducts())->all()->first();
        $this->delete($this->getRouteByName('category.delete', ['id' => $category->id]))
            ->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
