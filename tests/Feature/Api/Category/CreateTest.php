<?php

namespace Tests\Feature\Api\Category;

use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class CreateCategoryApiTest
 * @package Tests\Feature
 */
class CreateTest extends TestCase
{
    /**
     * @return void
     */
    public function testRequireParams()
    {
        $this->postJson($this->getRouteByName('category.create'))
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'errors' => [
                    'name' => [trans('validation.required', ['attribute' => 'name'])],
                ]

            ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccess()
    {
        $this->postJson($this->getRouteByName('category.create', ['name' => 'CategoryName4']))
            ->assertCreated()
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name'
                ]
            ]);
    }
}
