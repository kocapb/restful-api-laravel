<?php


namespace Tests\Feature\Api\Product;


use App\Models\{Category, Product};
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

/**
 * Class CreateTest
 * @package Tests\Feature\Api\Product
 */
class CreateTest extends TestCase
{
    /**
     * @return void
     */
    public function testRequireParams() : void
    {
        $this->postJson($this->getRouteByName('product.create'))
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'errors' => [
                    'name' => [trans('validation.required', ['attribute' => 'name'])],
                    'price' => [trans('validation.required', ['attribute' => 'price'])],
                    'category_ids' => [trans('validation.required', ['attribute' => 'category ids'])],
                ]

            ]);
    }

    /**
     * @return void
     */
    public function testWrongPriceParam() : void
    {
        $this->postJson($this->getRouteByName('product.create'), ['price' => 'price'])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'errors' => [
                    'price' => [trans('validation.money', ['attribute' => 'price'])],
                ]
            ]);
    }

    /**
     * @return void
     */
    public function testWrongCategoryIdsParam() : void
    {
        $this->postJson($this->getRouteByName('product.create'), ['category_ids' => 'category_ids'])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'errors' => [
                    'category_ids' => [trans('validation.array', ['attribute' => 'category ids'])],
                ]
            ]);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccessUnpublished()
    {
        $this->testSuccess(Product::UNPUBLISHED);
    }

    /**
     * @return void
     */
    public function testSuccessPublished() : void
    {
       $this->testSuccess(Product::PUBLISHED);
    }

    /**
     * @param bool $published
     */
    private function testSuccess(bool $published = Product::PUBLISHED) : void
    {
        /** @var Category $categories */
        $categories = App::make(Category::class)->get()->first();
        $productId = App::make(Product::class)->all()->max('id');

        $this->postJson($this->getRouteByName('product.create'), [
            'name' => 'ProductName',
            'price' => 10.0,
            'category_ids' => [$categories->id],
            'published' => $published,
        ])
            ->assertCreated()
            ->assertJson([
                    'data' => [
                        'id' => $productId + 1,
                        'name' => 'ProductName',
                        'price' => '10.0',
                        'published' => $published,
                        'categories' => [
                            [
                                'id' => $categories->id,
                                'name' => $categories->name
                            ]
                        ]
                    ]
                ]
            );
    }
}
