<?php


namespace Tests\Feature\Api\Product;


use Illuminate\Http\Request;
use App\Models\Product;
use App\Repositories\Criteria\Product\{PriceBetweenCriteria, ProductHasCategory, SearchByCategoryIdCriteria, SearchByNameCriteria};
use App\Repositories\Eloquent\ProductRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

/**
 * Class GetListTest
 * @package Tests\Feature\Api\Product
 */
class GetListTest extends TestCase
{
    /**
     * @return ProductRepository
     */
    public function getRepository()
    {
        return App::make(ProductRepository::class)
            ->with(['categories']);
    }

    /**
     * @return void
     */
    public function testWithoutSearchParamsSuccess() : void
    {
        /** @var Product[]|Collection|LengthAwarePaginator $products */
        $products = $this->getRepository()->paginate(config('paginate.per_page'));

        $this->getJson($this->getRouteByName('product.list'))
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => $this->prepareData($products),
            ]);
    }

    /**
     * @return void
     */
    public function testNameSearchParamSuccess() : void
    {
        $product = $this->getRandomProduct();
        /** @var string $name */
        $name = explode(' ', $product->name)[0];
        /** @var Product[]|Collection|LengthAwarePaginator $products */
        $products = $this->getRepository()
            ->pushCriteria(new SearchByNameCriteria($name))
            ->paginate(config('paginate.per_page'));

        $this->json(Request::METHOD_GET, $this->getRouteByName('product.list'), ['name' => $name])
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => $this->prepareData($products),
            ]);
    }

    /**
     * @return void
     */
    public function testCategorySearchParamSuccess() : void
    {
        $product = $this->getRepository()->pushCriteria(new ProductHasCategory())->all()->first();
        /** @var Product[]|Collection|LengthAwarePaginator $products */
        $products = $this->getRepository()
            ->pushCriteria(new SearchByCategoryIdCriteria($categoryId = $product->getCategories()->first()->id))
            ->paginate(config('paginate.per_page'));

        $this->json(Request::METHOD_GET, $this->getRouteByName('product.list'), ['category_id' => $categoryId])
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => $this->prepareData($products),
            ]);

    }

    /**
     * @return void
     */
    public function testPriceSearchParamSuccess() : void
    {
        $product = $this->getRandomProduct();
        $params = [
            'price_from' => floatval($product->price) - 0.1,
            'price_to' => floatval($product->price) + 1,
        ];
        $products = $this->getRepository()
            ->pushCriteria(new PriceBetweenCriteria($params['price_from'], $params['price_to']))
            ->paginate(config('paginate.per_page'));

        $this->json(Request::METHOD_GET, $this->getRouteByName('product.list'), $params)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => $this->prepareData($products),
            ]);
    }

    /**
     * @return Product
     */
    private function getRandomProduct() : Product
    {
        return $this->getRepository()->all()->random(1)->first();
    }

    /**
     * @param LengthAwarePaginator $products
     * @return array
     */
    private function prepareData(LengthAwarePaginator $products)
    {
        $data = [];
        /** @var Product[]|Collection $products */
        foreach ($products as $product)
        {
            $data[] = [
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'published' => $product->published,
                'categories' =>
                    ($product->getCategories()->count()) ? $product->getCategories()->only(['id', 'name'])->toArray() : [],
            ];
        }
        return $data;
    }
}
