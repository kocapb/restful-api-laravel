<?php


namespace Tests\Feature\Api\Product;

use App\Models\Product;
use App\Repositories\Eloquent\ProductRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    /**
     * @return mixed
     */
    public function getRepository()
    {
        return App::make(ProductRepository::class);
    }

    public function testSuccess() : void
    {
        /** @var Product $category */
        $product = $this->getRepository()->all()->first();
        $this->delete($this->getRouteByName('product.delete', ['id' => $product->id]))
            ->assertStatus(Response::HTTP_NO_CONTENT);
    }
}
