<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Http\Controllers\Api'], function () {

    /*
     * Product group routes
     */
    Route::group([
        'prefix' => 'product',
        'as' => 'product.'
    ], function () {
        Route::get('list', 'ProductController@getList')->name('list');
        Route::patch('edit/{id}', 'ProductController@edit')->name('edit');
        Route::post('create', 'ProductController@create')->name('create');
        Route::delete('delete/{id}', 'ProductController@delete')->name('delete');
    });

    /*
     * Category group routes
     */
    Route::group([
        'prefix' => 'category',
        'as' => 'category.'
    ], function () {
        Route::post('create', 'CategoryController@create')->name('create');
        Route::delete('delete/{id}', 'CategoryController@delete')->name('delete');
    });

});
