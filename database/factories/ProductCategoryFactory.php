<?php


namespace Database\Factories;


use App\Models\{Category, Product, ProductCategory};
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\App;


/**
 * Class ProductCategoryFactory
 * @package Database\Factories
 */
class ProductCategoryFactory extends Factory
{
    /** @var string  */
    protected $model = ProductCategory::class;

    /**
     * @return array
     */
    public function definition()
    {
        $productIds = App::make(Product::class)->all()->pluck('id');
        $categoryIds = App::make(Category::class)->all()->pluck('id');

        return [
            'product_id' => $this->faker->randomElement($productIds),
            'category_id' => $this->faker->randomElement($categoryIds)
        ];
    }
}
