<?php


namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class ProductFactory
 * @package Database\Factories
 */
class ProductFactory extends Factory
{
    /** @var string  */
    protected $model = Product::class;

    /**
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'price' => $this->faker->randomFloat(2,100, 1000),
            'published' => true,
        ];
    }

}
