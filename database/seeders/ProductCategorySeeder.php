<?php

namespace Database\Seeders;


use App\Models\{Product, ProductCategory};
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Exception;

/**
 * Class ProductCategorySeeder
 * @package Database\Seeders
 */
class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $productCount = App::make(Product::class)->all()->count();
        ProductCategory::factory(random_int(2, $productCount - 2))->create();
    }
}
