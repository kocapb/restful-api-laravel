<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /** @var int  */
    protected $count = 20;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::factory($this->count)->create();
    }
}
